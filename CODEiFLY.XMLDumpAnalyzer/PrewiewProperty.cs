﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CODEiFLY.XMLDumpAnalyzer
{
    public enum PrewiewProperty
    {
        Encoding,
        FirstTagName,
        DataHolderPath,
        FirstTagCount,
        FirstTagPosition,
        DataHolderTagName,
        FirstDataHolderTagPosition,
        DataHolderTagCount,
    }
}
