﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CODEiFLY.XMLDumpAnalyzer
{
    public class SimplePropertyViewModel
    {
        public string Name { get; set; }

        public string Value { get; set; }
    }
}
