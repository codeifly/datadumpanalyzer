﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace CODEiFLY.XMLDumpAnalyzer
{
    /// <summary>
    /// XML dump file analyzer
    /// </summary>
    public class Analyzer
    {
        /// <summary>
        /// Analyze the XML document
        /// </summary>
        /// <param name="xml">XML document to analyze</param>
        public void Analyze(XmlDocument xml)
        {
            try
            {
                if (xml == null)
                {
                    return;
                }
                var linqXML = XDocument.Load(new XmlNodeReader(xml));
                Document = new Document() { XMLDocumentUri = linqXML.BaseUri };
                //XElement e = (XElement)xml.FirstChild;            
                AnalyzeNode(linqXML.Descendants().FirstOrDefault(), null);
            }
            catch(Exception ex)
            {
                
            }
        }

        public Document Document { get; private set; }

        private void AnalyzeNode(XElement xmlNode, Node parentNode)
        {
            string v = xmlNode.Value;
            Node node = null;
            if (parentNode == null) 
            {
                node = new Node() { Name = xmlNode.Name.LocalName, Level = 0, Parent = null };
                Document.Nodes.Add(node);
            }
            else
            {
                node = parentNode.Nodes.FirstOrDefault(x => x.Name == xmlNode.Name);
                if (node == null)
                {
                    node = new Node() { Name = xmlNode.Name.LocalName, Level = parentNode.Level + 1, Parent = parentNode };                    
                    parentNode.Nodes.Add(node);
                }                
            }
            node.Occurrences++;
            if (xmlNode.Elements().Count() == 0 && !string.IsNullOrEmpty(xmlNode.Value))
            {
                node.HasElementValue = true;
                EvaluateValue(xmlNode.Value, node);
            }
            GetAttributes(xmlNode, node);
            foreach (XElement childNode in xmlNode.Elements())
            {
                if (childNode.NodeType == XmlNodeType.Element)
                {
                    AnalyzeNode(childNode, node);
                }
            }
        }

        private void EvaluateValue(string value, DataSample element)
        {
            element.AddDataSample(value.Length < 100 ? value : $"{value.Substring(0, 96)}...");
            int intValue;
            if (int.TryParse(value, out intValue))
            {
                element.AddType(intValue.GetType().Name);
            }
            else
            {
                double doubleValue;
                if (double.TryParse(value, out doubleValue))
                {
                    element.AddType(doubleValue.GetType().Name);
                }
                else
                {
                    bool boolValue;
                    if (bool.TryParse(value, out boolValue))
                    {
                        element.AddType(boolValue.GetType().Name);
                    }
                    else
                    {
                        element.AddType(typeof(string).Name);
                    }
                }
            }
        }

        private void GetAttributes(XElement xmlNode, Node node)
        {
            foreach (XAttribute xmlAttribute in xmlNode.Attributes())
            {
                Attribute attribute = node.Attributes.FirstOrDefault(x => x.Name == xmlAttribute.Name.LocalName);
                if (attribute == null)
                {
                    attribute = new Attribute() { Name = xmlAttribute.Name.LocalName, Node = node };
                    node.Attributes.Add(attribute);                    
                }
                attribute.Occurances++;
                if (String.IsNullOrEmpty(xmlAttribute.Value))
                {
                    attribute.CanEmpty = true;
                }
                else
                {
                    EvaluateValue(xmlAttribute.Value, attribute);
                }
            }
        }
    }
}
