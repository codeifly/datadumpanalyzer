﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace CODEiFLY.XMLDumpAnalyzer
{
    public class Node : DataSample
    {
        public Node()
        {
            this.Attributes = new ObservableCollection<Attribute>();
            this.Nodes = new ObservableCollection<Node>();
        }

        public string Name { get; set; }

        public int Level { get; set; }

        public Node Parent { get; set; }

        public int Occurrences { get; set; } = 0;

        public ObservableCollection<Node> Nodes { get; set; }

        public ObservableCollection<Attribute> Attributes { get; set; }

        /// <summary>
        /// This node has a simple element value (and not a complex type)
        /// </summary>
        public bool HasElementValue { get; set; }

        #region Properties for TreeView Binding

        /// <summary>
        /// Attributes list visibility
        /// </summary>
        public Visibility AttributeVisibility
        {
            get
            {
                return Attributes.Count > 0 ? Visibility.Visible : Visibility.Hidden; 
            }
        }

        /// <summary>
        /// List of first 5 attributes
        /// </summary>
        public string AttributeList
        {
            get
            {
                if (Attributes.Count == 0)
                {
                    return String.Empty;
                }
                int i = 1;
                StringBuilder sb = new StringBuilder();
                foreach (var item in Attributes)
                {
                    sb.Append($" {item.Name},");
                    if (i == 5 && Attributes.Count > 5)
                    {
                        sb.Append(" ....");
                        break;
                    }
                    i++;
                }
                sb.Remove(sb.Length - 1, 1);
                return sb.ToString();
            }
        }

        public Brush ElementColor
        {
            get
            {
                return HasElementValue ? Brushes.Red : Brushes.Blue;
            }
        }

        #endregion Properties for TreeView Binding
    }
}
