﻿using System.IO;
using System.Text;
using System.Linq;
using System.Xml;

namespace CODEiFLY.XMLDumpAnalyzer
{
    public class Previewer
    {
        public void SetContent(string fileName, int kiloBytesLimit = 100)
        {
            using (StreamReader sr = new StreamReader(fileName, true))
            {
                sr.Peek();
                DetectedEncoding = sr.CurrentEncoding;
                int byteLimit = kiloBytesLimit * 1024;
                char[] buffer = new char[byteLimit];
                sr.ReadBlock(buffer, 0, byteLimit);
                OriginalContent = new string(buffer);
                Analyze();
            }
        }

        private void Analyze()
        {
            int firstLt = OriginalContent.IndexOf("<");
            if (firstLt < 0)
            {
                AnalyzedContent = "This is not a XML file!";
                return;
            }            
            int firstGT = OriginalContent.IndexOf(">", firstLt);
            if (firstLt < 0 || firstGT < 0)
            {
                AnalyzedContent = "This is not a XML file!";
                return;
            }
            DataHolderPath = string.Empty;
            FirstTagPosition = firstLt;
            FirstTagName = OriginalContent.Substring(FirstTagPosition + 1, firstGT - 1);
            FirstTagCount = CountThisTag(FirstTagName);
            SearchFirstDataHolderTag();
            LastDataHolderCloserTagPosition = OriginalContent.LastIndexOf($"</{DataHolderTagName}>");

            AnalyzedContent = OriginalContent.Substring(FirstTagPosition, LastDataHolderCloserTagPosition + $"</{DataHolderTagName}>".Length);
            AnalyzedContent += $"</{FirstTagName}>";

            AnalyzedContent = GetAnalyzedXML(AnalyzedContent);
        }

        private string GetAnalyzedXML(string xml)
        {                        
            XMLDocument = new XmlDocument();
            try
            {
                // Load the XmlDocument with the XML.
                XMLDocument.LoadXml(xml);
                using (MemoryStream mStream = new MemoryStream())
                {
                    using (XmlTextWriter writer = new XmlTextWriter(mStream, Encoding.Unicode))
                    {
                        writer.Formatting = Formatting.Indented;
                        // Write the XML into a formatting XmlTextWriter
                        XMLDocument.WriteContentTo(writer);
                        writer.Flush();

                        mStream.Flush();
                        // Have to rewind the MemoryStream in order to read
                        // its contents.
                        mStream.Position = 0;
                        // Read MemoryStream contents into a StreamReader.
                        StreamReader sReader = new StreamReader(mStream);
                        // Extract the text from the StreamReader.
                        string formattedXml = sReader.ReadToEnd();
                        return formattedXml;
                    }
                }
            }
            catch (XmlException ex)
            {
                return ex.Message;
            }
        }

        private void SearchFirstDataHolderTag(int afterPosition = 0)
        {
            int firstLt = OriginalContent.IndexOf("<", afterPosition);
            if (firstLt < 0)
            {
                AnalyzedContent = "Data holder TAG is not found!";
                return;
            }
            int firstSpace = OriginalContent.IndexOf(" ", firstLt);
            int firstGT = OriginalContent.IndexOf(">", firstLt);
            if (firstLt < 0 || firstGT < 0)
            {
                AnalyzedContent = "Data holder TAG is not found!";
                return;
            }
            if (firstGT > firstSpace)
            {
                firstGT = firstSpace;
            }
            string tagName = OriginalContent.Substring(firstLt + 1, firstGT - firstLt - 1);            
            DataHolderPath += $"{tagName}\\";
            int closerTagPosition = OriginalContent.IndexOf($"</{tagName}>", firstGT);
            if (closerTagPosition > -1)
            {
                DataHolderTagName = tagName;
                DataHolderTagPosition = firstLt;
                DataHolderCount = CountThisTag($"/{DataHolderTagName}");
            }
            else
            {
                SearchFirstDataHolderTag(firstGT);
            }
        }

        private int CountThisTag(string tagName)
        {
            int count = 0;
            int lastPosition = -1;
            do
            {
                lastPosition = OriginalContent.IndexOf($"<{tagName}>", lastPosition + 1);
                if (lastPosition > -1)
                {
                    count++;
                }
            } while (lastPosition > -1);
            return count;
        }

        public string FirstTagName { get; private set; }

        public int FirstTagPosition { get; set; }

        public int FirstTagCount { get; private set; }

        public string DataHolderTagName { get; private set; }

        public int DataHolderTagPosition { get; private set; }

        public string DataHolderPath { get; private set; }

        public int DataHolderCount { get; private set; }

        public int LastDataHolderCloserTagPosition { get; private set; }

        public string AnalyzedContent { get; private set; }

        public string OriginalContent { get; private set; }

        public System.Xml.XmlDocument XMLDocument { get; private set; }

        public Encoding DetectedEncoding { get; private set; }
    }
}
