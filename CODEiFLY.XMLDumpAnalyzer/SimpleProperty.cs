﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CODEiFLY.XMLDumpAnalyzer
{
    public class SimpleProperty<T> : SimplePropertyViewModel
    {
        public T Key { get; set; }
    }
}
